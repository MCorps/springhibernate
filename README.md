# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Web application done with Spring Boot + Mysql + Hibernate
* 0.1
* [Based on this tutorial] (http://blog.netgloo.com/2014/10/27/using-mysql-in-spring-boot-via-spring-data-jpa-and-hibernate/)

### How do I get set up? ###

* Summary of set up: 

Just import the project into your Eclipse IDE (I have used STS (Spring Tool Suite)

You should run the gradle build when you are importing it

* Database configuration

Used a simple configuration in application.properties

```
#!java

spring.datasource.url = jdbc:mysql://localhost:3306/test
spring.datasource.username = miguel
spring.datasource.password = miguel

# Hibernate ddl auto (create, create-drop, update)
spring.jpa.hibernate.ddl-auto = update

```

Feel free to change it to match your own.

* How to run tests

No JUnit Test provided yet.

Manually tested by **CURL**:


```
#!bash

curl http://localhost:8080/create?"name=mike&email=mike@gmail.com"
curl http://localhost:8080/get-by-email?email=medina@hotmail.com
curl http://localhost:8080/delete?id=1
curl http://localhost:8080/update?"id=3&email=miguelangel.moreo@gmail.com&name=miguel_angel"
```


Note that names with white spaces are not working: for example "jon snow"

check with **Mysql Command Line Client** that the url´s are working:

```
#!bash

select * from users;
```

### Who do I talk to? ###

* miguelangel.moreo@gmail.com